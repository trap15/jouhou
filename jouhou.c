/*****************************************************************************
 *  jouhou.c                                                                 *
 *  A homegrown software based 3D engine.                                    *
 *- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*
 *  Copyright (C)2009 SquidMan (Alex Marshall)       <SquidMan72@gmail.com>  *
 *  Protected under the GNU GPLv2.                                           *
 *****************************************************************************/

#include <math.h>
#ifndef M_PI
#define M_PI 3.14159265358979323846264338327
#endif
#include "video.h"
#include "jouhou.h"

#define VERTEX_COUNT	(9001)
#define TRI_COUNT	(256)
#define STACK_SIZE	(256)
#define DEFAULT_COLOR	(0xFF)
#define SMALL_DOUBLE	(0.00001)

static int _zclip_on;
static double _zclip[2];
static double _rotation[3];
static double _translation[3];
static double _scaling[3];
static double _vertices[VERTEX_COUNT][3];
static int _colors[VERTEX_COUNT];
static int _current_color;
static int _vertexidx = 0;
static double _prerendered[TRI_COUNT][4][6];
static int _w, _h;
static int _init = JOUHOU_FALSE;
static int _type = JOUHOU_NONE;
static double _tris[TRI_COUNT][4][3];
static int _tricnt = 0;
static int _drawmode = JOUHOU_WIREFRAME;
static int _stereo = 0;

static double _stack[STACK_SIZE][3];
static int _stackloc = 0;

#define Deg2Rad(x)	(((x) * M_PI) /  180)
#define Rad2Deg(x)	(((x) *  180) / M_PI)

static void _Jouhou_ApplyRotation(double *x, double *y, double *z)
{
	if(_rotation[0] != 0) {
		double oldx = *x, oldz = *z;
		*x = (oldx * cos(Deg2Rad(_rotation[0]))) - (oldz * sin(Deg2Rad(_rotation[0])));
		*z = (oldx * sin(Deg2Rad(_rotation[0]))) + (oldz * cos(Deg2Rad(_rotation[0])));
	}
	if(_rotation[1] != 0) {
		double oldy = *y, oldz = *z;
		*y = (oldy * cos(Deg2Rad(_rotation[1]))) - (oldz * sin(Deg2Rad(_rotation[1])));
		*z = (oldy * sin(Deg2Rad(_rotation[1]))) + (oldz * cos(Deg2Rad(_rotation[1])));
	}
	if(_rotation[2] != 0) {
		double oldx = *x, oldy = *y;
		*x = (oldx * cos(Deg2Rad(_rotation[2]))) - (oldy * sin(Deg2Rad(_rotation[2])));
		*y = (oldx * sin(Deg2Rad(_rotation[2]))) + (oldy * cos(Deg2Rad(_rotation[2])));
	}
}

#define ZRATE (5)

static float _Jouhou_GetZScale(double z)
{
	if((z + ZRATE) <= 0)
		return 0;
	return ZRATE / (z + ZRATE);
}

static void _Jouhou_ClearTris()
{
	int i, l;
	for(i = 0; i < TRI_COUNT; i++) {
		for(l = 0; l < 3; l++) {
			_tris[i][l][0] = 0.0f;
			_tris[i][l][1] = 0.0f;
			_tris[i][l][2] = 0.0f;
		}
	}
	_tricnt = 0;
}

static void _Jouhou_ParseTri()
{
	int i, l;
	if((_vertexidx % 3) != 0)	/* We need a vertex count of % 3 for this to matter */
		_vertexidx -= (_vertexidx % 3);
	if(_vertexidx < 3)		/* We aren't going to get anything with less than 3 vertices... */
		return;
	for(_vertexidx /= 3; _vertexidx > 0; _vertexidx--, _tricnt++) {
		for(i = 0; i < 3; i++) {
			for(l = 0; l < 3; l++) {
				_tris[_tricnt][i][l] = _vertices[i + (3 * (_vertexidx - 1))][l];
			}
		}
		_tris[_tricnt][3][0] = _colors[3 * (_vertexidx - 1)];
	}
}

static void _Jouhou_ParseLine()
{
	int i, l;
	if((_vertexidx % 2) != 0)	/* We need a vertex count of % 2 for this to matter */
		_vertexidx -= (_vertexidx % 2);
	if(_vertexidx < 2)		/* We aren't going to get anything with less than 2 vertices... */
		return;
	for(_vertexidx /= 2; _vertexidx > 0; _vertexidx--, _tricnt++) {
		for(i = 0; i < 2; i++) {
			for(l = 0; l < 3; l++) {
				_tris[_tricnt][i][l] = _vertices[i + (2 * (_vertexidx - 1))][l];
				_tris[_tricnt][i + 1][l] = _vertices[i + (2 * (_vertexidx - 1))][l];
			}
		}
		_tris[_tricnt][3][0] = _colors[3 * (_vertexidx - 1)];
	}
}

static void _Jouhou_ParsePoint()
{
	int i, l;
	if(_vertexidx < 1)		/* Hurr, we need points */
		return;
	for(; _vertexidx > 0; _vertexidx--, _tricnt++) {
		for(i = 0; i < 3; i++) {
			for(l = 0; l < 3; l++) {
				_tris[_tricnt][i][l] = _vertices[_vertexidx - 1][l];
				_tris[_tricnt][i][l] = _vertices[_vertexidx - 1][l];
			}
		}
		_tris[_tricnt][3][0] = _colors[_vertexidx - 1];
	}
}

static void _Jouhou_ParseQuad()
{
	int i, l;
	if((_vertexidx % 4) != 0)	/* We need a vertex count of % 4 for this to matter */
		_vertexidx -= (_vertexidx % 4);
	if(_vertexidx < 4)		/* We aren't going to get anything with less than 4 vertices... */
		return;
	for(_vertexidx /= 4; _vertexidx > 0; _vertexidx--, _tricnt++) {
		for(i = 0; i < 3; i++) {
			for(l = 0; l < 3; l++) {
				_tris[_tricnt][i][l] = _vertices[i + (4 * (_vertexidx - 1))][l];
			}
		}
		_tris[_tricnt][3][0] = _colors[4 * (_vertexidx - 1)];
		_tricnt++;
		for(i = 1; i < 3; i++) {
			for(l = 0; l < 3; l++) {
				_tris[_tricnt][i][l] = _vertices[1 + i + (4 * (_vertexidx - 1))][l];
			}
		}
		for(l = 0; l < 3; l++) {
			_tris[_tricnt][0][l] = _vertices[4 * (_vertexidx - 1)][l];
		}
		_tris[_tricnt][3][0] = _colors[4 * (_vertexidx - 1)];
	}
}

void Jouhou_DrawMode(int mode)
{
	if(_init == JOUHOU_FALSE)
		return;
	_drawmode = mode;
}

#define STEREODIFF	(_stereo ? 5 : 0)
#define STEREODEG	(b ? STEREODIFF : -STEREODIFF)

void Jouhou_Render()
{
	if(_init == JOUHOU_FALSE)
		return;
	double x, y, z, a, oldx, oldz;
	int i, l, b;
	for(i = 0; i < _tricnt; i++) {
		for(l = 0; l < 3; l++) {
			for(b = 0; b < 2; b++) {
				x = _tris[i][l][0];
				y = _tris[i][l][1];
				z = _tris[i][l][2];
				oldx = x;
				oldz = z;
				x = (oldx * cos(Deg2Rad(STEREODEG))) - (oldz * sin(Deg2Rad(STEREODEG)));
				z = (oldx * sin(Deg2Rad(STEREODEG))) + (oldz * cos(Deg2Rad(STEREODEG)));
				if((_zclip_on && ((z < _zclip[0]) || (z > _zclip[1]))) || 
				   (z <= -ZRATE)){
					_prerendered[i][l][0+(b*3)] = -9001;
				}else{
					a = _Jouhou_GetZScale(z);
					x = ((x * a) * (_w / 2)) + (_w / 2);
					y = ((y * a) * (_h / 2)) + (_h / 2);
					_prerendered[i][l][0+(b*3)] = x;
					_prerendered[i][l][1+(b*3)] = _h - y;
					_prerendered[i][l][2+(b*3)] = z;
				}
			}
		}
		_prerendered[i][3][0] = _tris[i][3][0];
	}
	for(i = 0; i < _tricnt; i++) {
		if(_prerendered[i][0][0] == -9001)
			continue;
		if(_prerendered[i][1][0] == -9001)
			continue;
		if(_prerendered[i][2][0] == -9001)
			continue;
		if(_drawmode == JOUHOU_FILLED)
			Video_Draw(_prerendered[i], _prerendered[i][3][0]);
		else if(_drawmode == JOUHOU_WIREFRAME) {
			Video_Wires(_prerendered[i], _prerendered[i][3][0]);
		}
	}
	_Jouhou_ClearTris();
}

void Jouhou_ClearVertices()
{
	int i;
	if(_init == JOUHOU_FALSE)
		return;
	for(i = 0; i < VERTEX_COUNT; i++) {
		_vertices[i][0] = 0.0f;
		_vertices[i][1] = 0.0f;
		_vertices[i][2] = 0.0f;
		_colors[i] = DEFAULT_COLOR;
	}
	_vertexidx = 0;
}

void Jouhou_Identity()
{
	int i, l;
	if(_init == JOUHOU_FALSE)
		return;
	_zclip_on	=   1 ;
	_zclip[0]	= 0.0f;
	_zclip[1]	= 1.0f;
	_rotation[0]	= 0.0f;
	_rotation[1]	= 0.0f;
	_rotation[2]	= 0.0f;
	_translation[0]	= 0.0f;
	_translation[1]	= 0.0f;
	_translation[2]	= 0.0f;
	_scaling[0]	= 1.0f;
	_scaling[1]	= 1.0f;
	_scaling[2]	= 1.0f;
	for(i = 0; i < TRI_COUNT; i++) {
		for(l = 0; l < 3; l++) {
			_prerendered[i][l][0]	= 0.0f;
			_prerendered[i][l][1]	= 0.0f;
			_prerendered[i][l][2]	= 0.0f;
		}
	}
}

int Jouhou_Init(int x, int y, int stereoscopic)
{
	Jouhou_ClearVertices();
	Jouhou_Identity();
	_w = x;
	_h = y;
	_stereo = stereoscopic;
	_init = JOUHOU_TRUE;
	return _init;
}

void Jouhou_Begin(int type)
{
	if(_init == JOUHOU_FALSE)
		return;
	if(_type != JOUHOU_NONE)	/* If we've already begun something, just end it and start a new one. */
		Jouhou_End();
	_type = type;
}

void Jouhou_End()
{
	if(_init == JOUHOU_FALSE)
		return;
	switch(_type) {
		case JOUHOU_QUAD:
			_Jouhou_ParseQuad();
			break;
		case JOUHOU_TRIANGLE:
			_Jouhou_ParseTri();
			break;
		case JOUHOU_LINE:
			_Jouhou_ParseLine();
			break;
		case JOUHOU_POINT:
			_Jouhou_ParsePoint();
			break;
		default:
			break;
	}
	Jouhou_ClearVertices();
	_type = JOUHOU_NONE;
}

int Jouhou_Vertex3d(double x, double y, double z)
{
	double sx,sy,sz;
	if(_init == JOUHOU_FALSE)
		return -1;
	_Jouhou_ApplyRotation(&x, &y, &z);
	sx = _scaling[0];
	sy = _scaling[1];
	sz = _scaling[2];
	if(sx <= 0)
		sx = SMALL_DOUBLE;
	if(sy <= 0)
		sy = SMALL_DOUBLE;
	if(sz <= 0)
		sz = SMALL_DOUBLE;
	x = (x + _translation[0]) / sx;
	y = (y + _translation[1]) / sy;
	z = (z + _translation[2]) / sz;
	_vertices[_vertexidx][0] = x;
	_vertices[_vertexidx][1] = y;
	_vertices[_vertexidx][2] = z;
	_colors[_vertexidx] = _current_color;
	return _vertexidx++;
}

void Jouhou_Multiply3d(int vert, double x, double y, double z)
{
	if(_init == JOUHOU_FALSE)
		return;
	_vertices[vert][0] *= x;
	_vertices[vert][1] *= y;
	_vertices[vert][2] *= z;
}

void Jouhou_Scale3d(double x, double y, double z)
{
	if(_init == JOUHOU_FALSE)
		return;
	_scaling[0] = x;
	_scaling[1] = y;
	_scaling[2] = z;
}

void Jouhou_Translate3d(double x, double y, double z)
{
	if(_init == JOUHOU_FALSE)
		return;
	_translation[0] += x;
	_translation[1] += y;
	_translation[2] += z;
}

void Jouhou_Rotate3d(double x, double y, double z)
{
	if(_init == JOUHOU_FALSE)
		return;
	_rotation[0] += x;
	_rotation[1] += y;
	_rotation[2] += z;
}

void Jouhou_ZClip2d(int on, double zfar, double znear)
{
	if(_init == JOUHOU_FALSE)
		return;
	_zclip_on = on;
	_zclip[0] = zfar;
	_zclip[1] = znear;
}

void Jouhou_Color1i(int color)
{
	_current_color = color;
}

void Jouhou_PushInfo(int type)
{
	switch(type) {
		case JOUHOU_ALL_INFO:
			Jouhou_PushInfo(JOUHOU_ZCLIP_INFO);
			Jouhou_PushInfo(JOUHOU_SCALE_INFO);
			Jouhou_PushInfo(JOUHOU_TRANSLATE_INFO);
			Jouhou_PushInfo(JOUHOU_ROTATE_INFO);
			break;
		case JOUHOU_ZCLIP_INFO:
			_stack[_stackloc][0] = _zclip[0];
			_stack[_stackloc][1] = _zclip[1];
			_stack[_stackloc][2] = _zclip_on;
			_stackloc++;
			break;
		case JOUHOU_SCALE_INFO:
			_stack[_stackloc][0] = _scaling[0];
			_stack[_stackloc][1] = _scaling[1];
			_stack[_stackloc][2] = _scaling[2];
			_stackloc++;
			break;
		case JOUHOU_TRANSLATE_INFO:
			_stack[_stackloc][0] = _translation[0];
			_stack[_stackloc][1] = _translation[1];
			_stack[_stackloc][2] = _translation[2];
			_stackloc++;
			break;
		case JOUHOU_ROTATE_INFO:
			_stack[_stackloc][0] = _rotation[0];
			_stack[_stackloc][1] = _rotation[1];
			_stack[_stackloc][2] = _rotation[2];
			_stackloc++;
			break;
		default:
			return;
	}
}

void Jouhou_PopInfo(int type)
{
	switch(type) {
		case JOUHOU_ALL_INFO:
			Jouhou_PopInfo(JOUHOU_ZCLIP_INFO);
			Jouhou_PopInfo(JOUHOU_SCALE_INFO);
			Jouhou_PopInfo(JOUHOU_TRANSLATE_INFO);
			Jouhou_PopInfo(JOUHOU_ROTATE_INFO);
			break;
		case JOUHOU_ZCLIP_INFO:
			_stackloc--;
			_zclip[0] = _stack[_stackloc][0];
			_zclip[1] = _stack[_stackloc][1];
			_zclip_on = _stack[_stackloc][2];
			break;
		case JOUHOU_SCALE_INFO:
			_stackloc--;
			_scaling[0] = _stack[_stackloc][0];
			_scaling[1] = _stack[_stackloc][1];
			_scaling[2] = _stack[_stackloc][2];
			break;
		case JOUHOU_TRANSLATE_INFO:
			_stackloc--;
			_translation[0] = _stack[_stackloc][0];
			_translation[1] = _stack[_stackloc][1];
			_translation[2] = _stack[_stackloc][2];
			break;
		case JOUHOU_ROTATE_INFO:
			_stackloc--;
			_rotation[0] = _stack[_stackloc][0];
			_rotation[1] = _stack[_stackloc][1];
			_rotation[2] = _stack[_stackloc][2];
			break;
		default:
			return;
	}
}
