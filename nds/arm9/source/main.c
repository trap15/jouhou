#include <nds.h>
#include <math.h>

#include "video.h"
#include "jouhou.h"

#define MOVESPD	(0.04f)
#define ROTSPD	(2.0f)

#define SCALE	(4.0f)

#define VECOUNTC	(360 / 20)
#define VECOUNTS	(360 / 10)

#define Deg2Rad(x)	(((x) * M_PI) / 180)

void Do_Draw()
{
	Jouhou_Render();
}

void Calculate_Verts_Circle(float* verts, int count)
{
	int i;
	float degrees = (360.0f / (float)count);
	double x, y, oldx, oldy;
	for(i = 0; i < count; i++) {
		x = 0.0f;
		y = 1.0f;
		oldx = x;
		oldy = y;
		x = (oldx * cos(Deg2Rad(degrees * i))) - (oldy * sin(Deg2Rad(degrees * i)));
		y = (oldx * sin(Deg2Rad(degrees * i))) + (oldy * cos(Deg2Rad(degrees * i)));
		verts[(i * 3) + 0] = x;
		verts[(i * 3) + 1] = y;
		verts[(i * 3) + 2] = 0.5f;
	}
}

void Calculate_Verts_Sphere(float* verts, int count)
{
}

void Do_Update()
{
	int i;
	int vertcntc = VECOUNTC;
	float vertsc[VECOUNTC * 3];
	Calculate_Verts_Circle(vertsc, vertcntc);

	int vertcnts = VECOUNTS;
	float vertss[VECOUNTS * 3];
	Calculate_Verts_Sphere(vertss, vertcnts);

	Jouhou_Translate3d(1.5f,-1.5f, 0);
	Jouhou_Begin(JOUHOU_TRIFAN);
	{
		Jouhou_Vertex3d( 0.00f, 0.00f, 1.00f);
		for(i = 0; i < vertcntc; i++) {
			Jouhou_Vertex3d(vertsc[(i * 3) + 0], vertsc[(i * 3) + 1], vertsc[(i * 3) + 2]);
		}
	}
	Jouhou_End();
	Jouhou_Translate3d(-3.0f, 0, 0);
	Jouhou_Begin(JOUHOU_QUAD);
	{
		Jouhou_Vertex3d( 1.00f, 1.00f, 1.00f);
		Jouhou_Vertex3d(-1.00f, 1.00f, 1.00f);
		Jouhou_Vertex3d(-1.00f,-1.00f, 1.00f);
		Jouhou_Vertex3d( 1.00f,-1.00f, 1.00f);

		Jouhou_Vertex3d( 1.00f, 1.00f, 0.00f);
		Jouhou_Vertex3d( 1.00f,-1.00f, 0.00f);

		Jouhou_Vertex3d(-1.00f, 1.00f, 0.00f);
		Jouhou_Vertex3d(-1.00f,-1.00f, 0.00f);
	}
	Jouhou_End();
	Jouhou_Translate3d(0, 3.0f, 0);
	Jouhou_Begin(JOUHOU_TRIANGLE);
	{
		Jouhou_Vertex3d( 0.00f, 1.00f, 0.00f);
		Jouhou_Vertex3d(-1.00f, 0.00f, 1.00f);
		Jouhou_Vertex3d( 1.00f, 0.00f, 1.00f);
		Jouhou_Vertex3d( 0.00f, 0.00f,-1.00f);
		Jouhou_Vertex3d( 0.00f, 1.00f, 0.00f);
		Jouhou_Vertex3d(-1.00f, 0.00f, 1.00f);
	}
	Jouhou_End();
	Jouhou_Translate3d(3.0f, 0, 0);
	Jouhou_Translate3d(-1.5f,-1.5f, 0);
}

int Event_Handle()
{
	scanKeys();
	u32 btn = keysHeld() | keysDown();
	if(btn & KEY_LEFT) {
		Jouhou_Translate3d(-MOVESPD, 0, 0);
		iprintf("Left ");
	}
	if(btn & KEY_RIGHT) {
		Jouhou_Translate3d( MOVESPD, 0, 0);
		iprintf("Right ");
	}
	if(btn & KEY_DOWN) {
		Jouhou_Translate3d(0,-MOVESPD, 0);
		iprintf("Down ");
	}
	if(btn & KEY_UP) {
		Jouhou_Translate3d(0, MOVESPD, 0);
		iprintf("Up ");
	}
	if(btn & KEY_L) {
		Jouhou_Translate3d(0, 0,-MOVESPD);
		iprintf("L ");
	}
	if(btn & KEY_R) {
		Jouhou_Translate3d(0, 0, MOVESPD);
		iprintf("R ");
	}
	if(btn & KEY_Y) {
		Jouhou_Rotate3d   (-ROTSPD, 0, 0);
		iprintf("Y ");
	}
	if(btn & KEY_A) {
		Jouhou_Rotate3d   ( ROTSPD, 0, 0);
		iprintf("A ");
	}
	if(btn & KEY_B) {
		Jouhou_Rotate3d   (0,-ROTSPD, 0);
		iprintf("B ");
	}
	if(btn & KEY_X) {
		Jouhou_Rotate3d   (0, ROTSPD, 0);
		iprintf("X ");
	}
	if(btn & KEY_SELECT) {
		Jouhou_Rotate3d   (0, 0,-ROTSPD);
		iprintf("SELECT ");
	}
	if(btn & KEY_START) {
		Jouhou_Rotate3d   (0, 0, ROTSPD);
		iprintf("START ");
	}
	iprintf("%08X keys\n", btn);
	return 0;
}

int main(int argc, char *argv[])
{
	videoSetMode(MODE_FB0);
	vramSetBankA(VRAM_A_LCD);
	consoleDemoInit();
	
	int ret = Video_Init(256, 192);
	if(ret != 0)
		return 1;
	ret = Jouhou_Init(256, 192);
	if(ret != JOUHOU_TRUE)
		return 2;
	
	Jouhou_Identity();
	Jouhou_Scale3d(SCALE, SCALE, SCALE);
	Jouhou_ZClip1d(0, -2.0f);
	keysSetRepeat(1, 1);
	iprintf("Move: D-Pad.\n");
	iprintf("Zoom: L and R.\n");
	iprintf("Rotation:\n");
	iprintf("\tHorizontal: A     and Y\n");
	iprintf("\tVertical:   B     and X\n");
	iprintf("\tYaw:        START and SELECT\n");

	for(;;) {
		Do_Update();
		Do_Draw();
		Video_Update();
		Event_Handle();
	}
	
	Video_Quit();
	return(0);
}
