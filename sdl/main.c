
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>
#include <math.h>

#include "video.h"
#include "jouhou.h"

#define SCREEN_SIZE	(512)
#define STEREOSCOPIC	0
#define WIREFRAME	1

#define MOVESPD		(3.0f)
#define ROTSPD		(1.0f)

#define SCALE		(128.0f)

#define Deg2Rad(x)	(((x) * M_PI) / 180)

void _do_identity(void)
{
	Jouhou_Identity();
	Jouhou_Scale3d(SCALE, SCALE, SCALE);
	Jouhou_ZClip2d(0, 0.0f, 5.0f);
	Jouhou_Rotate3d(135.0f, -45.0f, 0.0f);
}

void Do_Update(void)
{
	int sw = 0;
	u8 r = 128, g = 128, b = 128;
	u32 i;
	/* Body */
	Jouhou_Begin(JOUHOU_TRIANGLE); {
		i = 0x80; Jouhou_Color1i((i << 16) | (i << 8) | i);
		Jouhou_Vertex3d(  0.0000f, 12.9375f, 90.0000f);
		Jouhou_Vertex3d(  0.0000f,  0.0000f,  0.0000f);
		Jouhou_Vertex3d( 24.3125f, 12.9375f,  0.0000f);
		i = 0x90; Jouhou_Color1i((i << 16) | (i << 8) | i);
		Jouhou_Vertex3d(  0.0000f, 12.9375f, 90.0000f);
		Jouhou_Vertex3d(  0.0000f,  0.0000f,  0.0000f);
		Jouhou_Vertex3d(-24.3125f, 12.9375f,  0.0000f);
		i = 0x90; Jouhou_Color1i((i << 16) | (i << 8) | i);
		Jouhou_Vertex3d(  0.0000f, 12.9375f, 90.0000f);
		Jouhou_Vertex3d(  0.0000f, 34.0519f,  0.0000f);
		Jouhou_Vertex3d( 24.3125f, 12.9375f,  0.0000f);
		i = 0x80; Jouhou_Color1i((i << 16) | (i << 8) | i);
		Jouhou_Vertex3d(  0.0000f, 12.9375f, 90.0000f);
		Jouhou_Vertex3d(  0.0000f, 34.0519f,  0.0000f);
		Jouhou_Vertex3d(-24.3125f, 12.9375f,  0.0000f);
		i = 0x88; Jouhou_Color1i((i << 16) | (i << 8) | i);
		Jouhou_Vertex3d(-24.3125f, 12.9375f,  0.0000f);
		Jouhou_Vertex3d(  0.0000f,  0.0000f,  0.0000f);
		Jouhou_Vertex3d( 24.3125f, 12.9375f,  0.0000f);
		i = 0x78; Jouhou_Color1i((i << 16) | (i << 8) | i);
		Jouhou_Vertex3d(-24.3125f, 12.9375f,  0.0000f);
		Jouhou_Vertex3d(  0.0000f, 24.8750f,-30.0000f);
		Jouhou_Vertex3d( 24.3125f, 12.9375f,  0.0000f);
		i = 0x80; Jouhou_Color1i((i << 16) | (i << 8) | i);
		Jouhou_Vertex3d(  0.0000f, 34.0519f,  0.0000f);
		Jouhou_Vertex3d(  0.0000f, 24.8750f,-30.0000f);
		Jouhou_Vertex3d( 24.3125f, 12.9375f,  0.0000f);
		i = 0x90; Jouhou_Color1i((i << 16) | (i << 8) | i);
		Jouhou_Vertex3d(  0.0000f, 34.0519f,  0.0000f);
		Jouhou_Vertex3d(  0.0000f, 24.8750f,-30.0000f);
		Jouhou_Vertex3d(-24.3125f, 12.9375f,  0.0000f);
	} Jouhou_End();
	/* Thrusters/Lasers */
	r = g = 0x20;
	Jouhou_Begin(JOUHOU_TRIANGLE); {
		/* Right */
		b = 0xC0; Jouhou_Color1i((r << 16) | (g << 8) | b);
		Jouhou_Vertex3d( 27.0000f, 31.1250f,-49.8125f);
		Jouhou_Vertex3d( 24.3125f, 22.6250f,  0.0000f);
		Jouhou_Vertex3d( 26.8125f, 15.6132f, -7.3284f);
		b = 0xA0; Jouhou_Color1i((r << 16) | (g << 8) | b);
		Jouhou_Vertex3d( 27.0000f, 31.1250f,-49.8125f);
		Jouhou_Vertex3d( 24.3125f, 22.6250f,  0.0000f);
		Jouhou_Vertex3d( 21.9375f, 15.6132f, -7.3284f);
		b = 0xC0; Jouhou_Color1i((r << 16) | (g << 8) | b);
		Jouhou_Vertex3d( 27.0000f, 31.1250f,-49.8125f);
		Jouhou_Vertex3d( 24.3125f, 12.9375f,-10.1250f);
		Jouhou_Vertex3d( 21.9375f, 15.6132f, -7.3284f);
		b = 0xA0; Jouhou_Color1i((r << 16) | (g << 8) | b);
		Jouhou_Vertex3d( 27.0000f, 31.1250f,-49.8125f);
		Jouhou_Vertex3d( 24.3125f, 12.9375f,-10.1250f);
		Jouhou_Vertex3d( 26.8125f, 15.6132f, -7.3284f);
		b = 0xA0; Jouhou_Color1i((r << 16) | (g << 8) | b);
		Jouhou_Vertex3d( 24.3125f, 12.9375f,  0.0000f);
		Jouhou_Vertex3d( 24.3125f, 22.6250f,  0.0000f);
		Jouhou_Vertex3d( 26.8125f, 15.6132f, -7.3284f);
		b = 0xC0; Jouhou_Color1i((r << 16) | (g << 8) | b);
		Jouhou_Vertex3d( 24.3125f, 12.9375f,  0.0000f);
		Jouhou_Vertex3d( 24.3125f, 22.6250f,  0.0000f);
		Jouhou_Vertex3d( 21.9375f, 15.6132f, -7.3284f);
		b = 0xA0; Jouhou_Color1i((r << 16) | (g << 8) | b);
		Jouhou_Vertex3d( 24.3125f, 12.9375f,  0.0000f);
		Jouhou_Vertex3d( 24.3125f, 12.9375f,-10.1250f);
		Jouhou_Vertex3d( 21.9375f, 15.6132f, -7.3284f);
		b = 0xC0; Jouhou_Color1i((r << 16) | (g << 8) | b);
		Jouhou_Vertex3d( 24.3125f, 12.9375f,  0.0000f);
		Jouhou_Vertex3d( 24.3125f, 12.9375f,-10.1250f);
		Jouhou_Vertex3d( 26.8125f, 15.6132f, -7.3284f);
		b = 0xC0; Jouhou_Color1i((r << 16) | (g << 8) | b);
		Jouhou_Vertex3d( 24.3125f, 12.9375f,  0.0000f);
		Jouhou_Vertex3d( 24.3125f,  7.0000f,  7.0938f);
		Jouhou_Vertex3d( 27.1250f, 10.4735f,  7.0938f);
		b = 0xA0; Jouhou_Color1i((r << 16) | (g << 8) | b);
		Jouhou_Vertex3d( 24.3125f, 12.9375f,  0.0000f);
		Jouhou_Vertex3d( 24.3125f,  7.0000f,  7.0938f);
		Jouhou_Vertex3d( 23.0625f, 10.4735f,  7.0938f);
		b = 0xA0; Jouhou_Color1i((r << 16) | (g << 8) | b);
		Jouhou_Vertex3d( 24.3125f, 12.9375f,  0.0000f);
		Jouhou_Vertex3d( 24.3125f, 12.9375f,  7.0938f);
		Jouhou_Vertex3d( 27.1250f, 10.4735f,  7.0938f);
		b = 0xC0; Jouhou_Color1i((r << 16) | (g << 8) | b);
		Jouhou_Vertex3d( 24.3125f, 12.9375f,  0.0000f);
		Jouhou_Vertex3d( 24.3125f, 12.9375f,  7.0938f);
		Jouhou_Vertex3d( 23.0625f, 10.4735f,  7.0938f);
		b = 0xA0; Jouhou_Color1i((r << 16) | (g << 8) | b);
		Jouhou_Vertex3d( 24.3125f,  7.0000f, 17.0938f);
		Jouhou_Vertex3d( 24.3125f,  7.0000f,  7.0938f);
		Jouhou_Vertex3d( 27.1250f, 10.4735f,  7.0938f);
		b = 0xC0; Jouhou_Color1i((r << 16) | (g << 8) | b);
		Jouhou_Vertex3d( 24.3125f,  7.0000f, 17.0938f);
		Jouhou_Vertex3d( 24.3125f,  7.0000f,  7.0938f);
		Jouhou_Vertex3d( 23.0625f, 10.4735f,  7.0938f);
		b = 0xC0; Jouhou_Color1i((r << 16) | (g << 8) | b);
		Jouhou_Vertex3d( 24.3125f,  7.0000f, 17.0938f);
		Jouhou_Vertex3d( 24.3125f, 12.9375f,  7.0938f);
		Jouhou_Vertex3d( 27.1250f, 10.4735f,  7.0938f);
		b = 0xA0; Jouhou_Color1i((r << 16) | (g << 8) | b);
		Jouhou_Vertex3d( 24.3125f,  7.0000f, 17.0938f);
		Jouhou_Vertex3d( 24.3125f, 12.9375f,  7.0938f);
		Jouhou_Vertex3d( 23.0625f, 10.4735f,  7.0938f);

		/* Left */
		b = 0xC0; Jouhou_Color1i((r << 16) | (g << 8) | b);
		Jouhou_Vertex3d(-27.0000f, 31.1250f,-49.8125f);
		Jouhou_Vertex3d(-24.3125f, 22.6250f,  0.0000f);
		Jouhou_Vertex3d(-26.8125f, 15.6132f, -7.3284f);
		b = 0xA0; Jouhou_Color1i((r << 16) | (g << 8) | b);
		Jouhou_Vertex3d(-27.0000f, 31.1250f,-49.8125f);
		Jouhou_Vertex3d(-24.3125f, 22.6250f,  0.0000f);
		Jouhou_Vertex3d(-21.9375f, 15.6132f, -7.3284f);
		b = 0xC0; Jouhou_Color1i((r << 16) | (g << 8) | b);
		Jouhou_Vertex3d(-27.0000f, 31.1250f,-49.8125f);
		Jouhou_Vertex3d(-24.3125f, 12.9375f,-10.1250f);
		Jouhou_Vertex3d(-21.9375f, 15.6132f, -7.3284f);
		b = 0xA0; Jouhou_Color1i((r << 16) | (g << 8) | b);
		Jouhou_Vertex3d(-27.0000f, 31.1250f,-49.8125f);
		Jouhou_Vertex3d(-24.3125f, 12.9375f,-10.1250f);
		Jouhou_Vertex3d(-26.8125f, 15.6132f, -7.3284f);
		b = 0xA0; Jouhou_Color1i((r << 16) | (g << 8) | b);
		Jouhou_Vertex3d(-24.3125f, 12.9375f,  0.0000f);
		Jouhou_Vertex3d(-24.3125f, 22.6250f,  0.0000f);
		Jouhou_Vertex3d(-26.8125f, 15.6132f, -7.3284f);
		b = 0xC0; Jouhou_Color1i((r << 16) | (g << 8) | b);
		Jouhou_Vertex3d(-24.3125f, 12.9375f,  0.0000f);
		Jouhou_Vertex3d(-24.3125f, 22.6250f,  0.0000f);
		Jouhou_Vertex3d(-21.9375f, 15.6132f, -7.3284f);
		b = 0xA0; Jouhou_Color1i((r << 16) | (g << 8) | b);
		Jouhou_Vertex3d(-24.3125f, 12.9375f,  0.0000f);
		Jouhou_Vertex3d(-24.3125f, 12.9375f,-10.1250f);
		Jouhou_Vertex3d(-21.9375f, 15.6132f, -7.3284f);
		b = 0xC0; Jouhou_Color1i((r << 16) | (g << 8) | b);
		Jouhou_Vertex3d(-24.3125f, 12.9375f,  0.0000f);
		Jouhou_Vertex3d(-24.3125f, 12.9375f,-10.1250f);
		Jouhou_Vertex3d(-26.8125f, 15.6132f, -7.3284f);
		b = 0xC0; Jouhou_Color1i((r << 16) | (g << 8) | b);
		Jouhou_Vertex3d(-24.3125f, 12.9375f,  0.0000f);
		Jouhou_Vertex3d(-24.3125f,  7.0000f,  7.0938f);
		Jouhou_Vertex3d(-27.1250f, 10.4735f,  7.0938f);
		b = 0xA0; Jouhou_Color1i((r << 16) | (g << 8) | b);
		Jouhou_Vertex3d(-24.3125f, 12.9375f,  0.0000f);
		Jouhou_Vertex3d(-24.3125f,  7.0000f,  7.0938f);
		Jouhou_Vertex3d(-23.0625f, 10.4735f,  7.0938f);
		b = 0xA0; Jouhou_Color1i((r << 16) | (g << 8) | b);
		Jouhou_Vertex3d(-24.3125f, 12.9375f,  0.0000f);
		Jouhou_Vertex3d(-24.3125f, 12.9375f,  7.0938f);
		Jouhou_Vertex3d(-27.1250f, 10.4735f,  7.0938f);
		b = 0xC0; Jouhou_Color1i((r << 16) | (g << 8) | b);
		Jouhou_Vertex3d(-24.3125f, 12.9375f,  0.0000f);
		Jouhou_Vertex3d(-24.3125f, 12.9375f,  7.0938f);
		Jouhou_Vertex3d(-23.0625f, 10.4735f,  7.0938f);
		b = 0xA0; Jouhou_Color1i((r << 16) | (g << 8) | b);
		Jouhou_Vertex3d(-24.3125f,  7.0000f, 17.0938f);
		Jouhou_Vertex3d(-24.3125f,  7.0000f,  7.0938f);
		Jouhou_Vertex3d(-27.1250f, 10.4735f,  7.0938f);
		b = 0xC0; Jouhou_Color1i((r << 16) | (g << 8) | b);
		Jouhou_Vertex3d(-24.3125f,  7.0000f, 17.0938f);
		Jouhou_Vertex3d(-24.3125f,  7.0000f,  7.0938f);
		Jouhou_Vertex3d(-23.0625f, 10.4735f,  7.0938f);
		b = 0xC0; Jouhou_Color1i((r << 16) | (g << 8) | b);
		Jouhou_Vertex3d(-24.3125f,  7.0000f, 17.0938f);
		Jouhou_Vertex3d(-24.3125f, 12.9375f,  7.0938f);
		Jouhou_Vertex3d(-27.1250f, 10.4735f,  7.0938f);
		b = 0xA0; Jouhou_Color1i((r << 16) | (g << 8) | b);
		Jouhou_Vertex3d(-24.3125f,  7.0000f, 17.0938f);
		Jouhou_Vertex3d(-24.3125f, 12.9375f,  7.0938f);
		Jouhou_Vertex3d(-23.0625f, 10.4735f,  7.0938f);
	} Jouhou_End();
	/* Wings */
	Jouhou_Begin(JOUHOU_TRIANGLE); {
		/* Right */
		i = 0x80; Jouhou_Color1i((i << 16) | (i << 8) | i);
		Jouhou_Vertex3d( 24.3125f, 12.9375f,  0.0000f);
		Jouhou_Vertex3d(119.3125f, 12.9375f,-39.4063f);
		Jouhou_Vertex3d( 63.8958f, 18.7500f,- 6.5677f);
		i = 0x90; Jouhou_Color1i((i << 16) | (i << 8) | i);
		Jouhou_Vertex3d( 24.3125f, 12.9375f,  0.0000f);
		Jouhou_Vertex3d(119.3125f, 12.9375f,-39.4063f);
		Jouhou_Vertex3d( 63.8958f,  7.1250f,- 6.5677f);
		i = 0x90; Jouhou_Color1i((i << 16) | (i << 8) | i);
		Jouhou_Vertex3d( 48.0625f, 12.9375f, 19.7031f);
		Jouhou_Vertex3d(119.3125f, 12.9375f,-39.4063f);
		Jouhou_Vertex3d( 63.8958f, 18.7500f,- 6.5677f);
		i = 0x80; Jouhou_Color1i((i << 16) | (i << 8) | i);
		Jouhou_Vertex3d( 48.0625f, 12.9375f, 19.7031f);
		Jouhou_Vertex3d(119.3125f, 12.9375f,-39.4063f);
		Jouhou_Vertex3d( 63.8958f,  7.1250f,- 6.5677f);
		i = 0x88; Jouhou_Color1i((i << 16) | (i << 8) | i);
		Jouhou_Vertex3d( 48.0625f, 12.9375f, 19.7031f);
		Jouhou_Vertex3d( 24.3125f, 12.9375f,  0.0000f);
		Jouhou_Vertex3d( 63.8958f, 18.7500f,- 6.5677f);
		i = 0x88; Jouhou_Color1i((i << 16) | (i << 8) | i);
		Jouhou_Vertex3d( 48.0625f, 12.9375f, 19.7031f);
		Jouhou_Vertex3d( 24.3125f, 12.9375f,  0.0000f);
		Jouhou_Vertex3d( 63.8958f,  7.1250f,- 6.5677f);

		/* Left */
		i = 0x80; Jouhou_Color1i((i << 16) | (i << 8) | i);
		Jouhou_Vertex3d(- 24.3125f, 12.9375f,  0.0000f);
		Jouhou_Vertex3d(-119.3125f, 12.9375f,-39.4063f);
		Jouhou_Vertex3d(- 63.8958f, 18.7500f,- 6.5677f);
		i = 0x90; Jouhou_Color1i((i << 16) | (i << 8) | i);
		Jouhou_Vertex3d(- 24.3125f, 12.9375f,  0.0000f);
		Jouhou_Vertex3d(-119.3125f, 12.9375f,-39.4063f);
		Jouhou_Vertex3d(- 63.8958f,  7.1250f,- 6.5677f);
		i = 0x90; Jouhou_Color1i((i << 16) | (i << 8) | i);
		Jouhou_Vertex3d(- 48.0625f, 12.9375f, 19.7031f);
		Jouhou_Vertex3d(-119.3125f, 12.9375f,-39.4063f);
		Jouhou_Vertex3d(- 63.8958f, 18.7500f,- 6.5677f);
		i = 0x80; Jouhou_Color1i((i << 16) | (i << 8) | i);
		Jouhou_Vertex3d(- 48.0625f, 12.9375f, 19.7031f);
		Jouhou_Vertex3d(-119.3125f, 12.9375f,-39.4063f);
		Jouhou_Vertex3d(- 63.8958f,  7.1250f,- 6.5677f);
		i = 0x88; Jouhou_Color1i((i << 16) | (i << 8) | i);
		Jouhou_Vertex3d(- 48.0625f, 12.9375f, 19.7031f);
		Jouhou_Vertex3d(- 24.3125f, 12.9375f,  0.0000f);
		Jouhou_Vertex3d(- 63.8958f, 18.7500f,- 6.5677f);
		i = 0x88; Jouhou_Color1i((i << 16) | (i << 8) | i);
		Jouhou_Vertex3d(- 48.0625f, 12.9375f, 19.7031f);
		Jouhou_Vertex3d(- 24.3125f, 12.9375f,  0.0000f);
		Jouhou_Vertex3d(- 63.8958f,  7.1250f,- 6.5677f);
	} Jouhou_End();
}

int Event_Handle()
{
	static double xmov = 0.0f, ymov = 0.0f, zmov = 0.0f;
	static double xrot = 0.0f, yrot = 0.0f, zrot = 0.0f;
	SDL_Event e;
	while(SDL_PollEvent(&e)) {
		switch(e.type) {
			case SDL_KEYDOWN:
				switch(e.key.keysym.sym) {
					case SDLK_s:
						xmov = -MOVESPD;
						break;
					case SDLK_f:
						xmov =  MOVESPD;
						break;
					case SDLK_e:
						ymov = -MOVESPD;
						break;
					case SDLK_d:
						ymov =  MOVESPD;
						break;
					case SDLK_w:
						zmov = -MOVESPD;
						break;
					case SDLK_r:
						zmov =  MOVESPD;
						break;
					case SDLK_j:
						xrot = - ROTSPD;
						break;
					case SDLK_l:
						xrot =   ROTSPD;
						break;
					case SDLK_i:
						yrot = - ROTSPD;
						break;
					case SDLK_k:
						yrot =   ROTSPD;
						break;
					case SDLK_u:
						zrot = - ROTSPD;
						break;
					case SDLK_o:
						zrot =   ROTSPD;
						break;
					case SDLK_RETURN:
						_do_identity();
						break;
					default:
						break;
				}
				break;
			case SDL_KEYUP:
				switch(e.key.keysym.sym) {
					case SDLK_s:
					case SDLK_f:
						xmov = 0;
						break;
					case SDLK_e:
					case SDLK_d:
						ymov = 0;
						break;
					case SDLK_w:
					case SDLK_r:
						zmov = 0;
						break;
					case SDLK_j:
					case SDLK_l:
						xrot = 0;
						break;
					case SDLK_i:
					case SDLK_k:
						yrot = 0;
						break;
					case SDLK_u:
					case SDLK_o:
						zrot = 0;
						break;
					default:
						break;
				}
				break;
			case SDL_MOUSEBUTTONDOWN:
			case SDL_QUIT:
				return 1;
			default:
				break;
		}
	}
	Jouhou_Translate3d(xmov, ymov, zmov);
	Jouhou_Rotate3d(xrot, yrot, zrot);
	return 0;
}

int main(int argc, char *argv[])
{
	int ret = Video_Init(SCREEN_SIZE, SCREEN_SIZE, STEREOSCOPIC);
	if(ret != 0)
		return 1;
	ret = Jouhou_Init(SCREEN_SIZE, SCREEN_SIZE, STEREOSCOPIC);
	if(ret != JOUHOU_TRUE)
		return 2;
#if !WIREFRAME
	Jouhou_DrawMode(JOUHOU_FILLED);
#endif
	_do_identity();
	
	do {
		Do_Update();
		Jouhou_Render();
		Video_Update();
		ret = Event_Handle();
	} while(!ret);
	
	Video_Quit();
	return(0);
}

