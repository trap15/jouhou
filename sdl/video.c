/*****************************************************************************
 *  video.c                                                                  *
 *  Video Abstraction functions.                                             *
 *- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*
 *  Copyright (C)2009-2010 trap15 (Alex Marshall)    <SquidMan72@gmail.com>  *
 *  Protected under the GNU GPLv2.                                           *
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <SDL/SDL.h>
#include <math.h>
#include <float.h>
#include "video.h"

#define VIDEOFLAGS	(SDL_HWSURFACE)
static SDL_Surface* screen;

static u32* vbmp;
static double* zbuffer;

static int double_buffer = 0;
#define VIDEO_THROTTLE	0

static int _stereo = 0;
static int WIDTH, HEIGHT;
static int SCREEN_W, SCREEN_H;

#define max(a,b)	(((a) > (b)) ?  (a) : (b))
#define min(a,b)	(((a) < (b)) ?  (a) : (b))
#define m_abs(a)	(((a) <  0)  ? -(a) : (a))
#define sign(a)		(((a) <  0)  ? - 1  : (((a) > 0) ? 1 : 0))

int Video_Init(int w, int h, int stereoscopic)
{
	if(SDL_Init(SDL_INIT_EVERYTHING) == -1) {
		fprintf(stderr, "Could not initialize SDL: %s.\n", SDL_GetError());
		return 1;
	}
	WIDTH = w;
	HEIGHT = h;
	_stereo = stereoscopic;
	if(stereoscopic) {
		w *= 2;
		h += 40;
	}
	screen = SDL_SetVideoMode(w, h, 32, VIDEOFLAGS);
	if(screen == NULL) {
		fprintf(stderr, "Couldn't set video mode: %s.\n", SDL_GetError());
		return 1;
	}
	SDL_WM_SetCaption("Jouhou", NULL);
	SCREEN_W = w;
	SCREEN_H = h;
	vbmp = calloc(SCREEN_W, SCREEN_H * 4);
	zbuffer = calloc(SCREEN_W, SCREEN_H * sizeof(double));
	return 0;
}	

void Video_Quit()
{
	SDL_FreeSurface(screen);
	free(vbmp);
	free(zbuffer);
}

static void put_pixel(u32* scrn, s16 x, s16 y, u32 color, int side)
{
	if((x >= WIDTH) || (y >= HEIGHT) || (x < 0) || (y < 0))
		return;
	scrn[x + (side ? WIDTH : 0) + (SCREEN_W * y)] = color;
}

static int draw_line(u32* scrn, s16 x1, s16 y1, s16 x2, s16 y2, u32 color, int side)
{
	int d;
	int x;
	int y;
	int ax;
	int ay;
	int sx;
	int sy;
	int dx;
	int dy;
	
	dx = x2 - x1;
	ax = abs(dx) << 1;
	sx = sign(dx);
	
	dy = y2 - y1;
	ay = abs(dy) << 1;
	sy = sign(dy);
	
	x = x1;
	y = y1;
	
	if(ax > ay) {
		d = ay - (ax >> 1);
		for(;;) {
			put_pixel(scrn, x, y, color, side);
			if(x == x2)
				return 0;
			if(d >= 0) {
				y += sy;
				d -= ax;
			}
			x += sx;
			d += ay;
		}
	}else{
		d = ax - (ay >> 1);
		for(;;) {
			put_pixel(scrn, x, y, color, side);
			if(y == y2)
				return 0;
			if(d >= 0) {
				x += sx;
				d -= ay;
			}
			y += sy;
			d += ax;
		}
	}	
	
	return 1;
}

static void Video_Line(int x1, int y1, int x2, int y2, u32 color, int side)
{
	if((x1 > WIDTH) && (x2 > WIDTH)) {
		return;
	}
	if((y1 > HEIGHT) && (y2 > HEIGHT)) {
		return;
	}
	if((x1 < 0) && (x2 < 0)) {
		return;
	}
	if((y1 < 0) && (y2 < 0)) {
		return;
	}
	draw_line(vbmp, x1, y1, x2, y2, color, side);
}

#define SWAP(x1, x2) \
	tmp = x1; \
	x1 = x2; \
	x2 = tmp;

#define SWAPPING(x1, y1, z1, x2, y2, z2) \
	if(_vtx_lt(x1, y1, z1, x2, y2, z2)) { \
		SWAP(x2, x1); \
		SWAP(y2, y1); \
		SWAP(z2, z1); \
	}

int _vtx_lt(double x1, double y1, double z1, double x2, double y2, double z2)
{
	if(y1 < y2)
		return 1;
	else if(y1 == y2) {
		if(x1 < x2)
			return 1;
		else if(x1 == x2)
			return z1 <= z2;
	}
	return 0;
}

#if 0
#define FILLBMP \
	bmp[(int)(x + xadj + (y * SCREEN_W))] = \
		(((int)((z + 3.0f) * 255.0f / 6.0f) & 0xFF) << 16) | \
		(((int)((z + 3.0f) * 255.0f / 6.0f) & 0xFF) <<  8) | \
		(((int)((z + 3.0f) * 255.0f / 6.0f) & 0xFF) <<  0)
#else
#define FILLBMP \
	bmp[(int)(x + xadj + (y * SCREEN_W))] = fill
#endif

static void draw_toptriangle(u32* bmp, double* wbuf,
			  double x1, double y1, double z1,
			  double x2, double y2, double z2,
			  double x3, double y3, double z3,
			  u32 fill, int side)
{
	double dy, dx_left, dx_right, x_left, x_right, x, dz_left, dz_right, z_left, z_right, z;
	int y, yend;
	double xadj = side * WIDTH;
	yend = min(y2, HEIGHT);
	dy = y2 - y1;
	dz_left = (z2 - z1) / dy;
	dz_right = (z3 - z1) / dy;
	dx_left = (x2 - x1) / dy;
	dx_right = (x3 - x1) / dy;
	x_left = x1 + (-min(y1, 0) * dx_left);
	x_right = x1 + (-min(y1, 0) * dx_right);
	z_left = z1 + (-min(y1, 0) * dz_left);
	z_right = z1 + (-min(y1, 0) * dz_right);
	for(y = max(y1, 0); y < yend; y++) {
		for(x = max(x_left, 0); x < min(x_right, WIDTH); x++) {
			z = z_left + (x - x_left)*(z_right - z_left)/(x_right - x_left);
			if(z < wbuf[(int)(x + xadj + (y * SCREEN_W))]) {
				FILLBMP;
				wbuf[(int)(x + xadj + (y * SCREEN_W))] = z;
			}
		}
		x_left += dx_left;
		x_right += dx_right;
		z_left += dz_left;
		z_right += dz_right;
	}
}

static void draw_bottomtriangle(u32* bmp, double* wbuf,
			  double x1, double y1, double z1,
			  double x2, double y2, double z2,
			  double x3, double y3, double z3,
			  u32 fill, int side)
{
	double dy, dx_left, dx_right, x_left, x_right, x, dz_left, dz_right, z_left, z_right, z;
	int y, yend;
	double xadj = side * WIDTH;
	yend = min(y3, HEIGHT);
	dy = y3 - y1;
	dz_left = (z3 - z1) / dy;
	dz_right = (z3 - z2) / dy;
	dx_left = (x3 - x1) / dy;
	dx_right = (x3 - x2) / dy;
	x_left = x1 + (-min(y1, 0) * dx_left);
	x_right = x2 + (-min(y1, 0) * dx_right);
	z_left = z1 + (-min(y1, 0) * dz_left);
	z_right = z2 + (-min(y1, 0) * dz_right);
	for(y = max(y1, 0); y < yend; y++) {
		for(x = max(x_left, 0); x < min(x_right, WIDTH); x++) {
			z = z_left + (x - x_left)*(z_right - z_left)/(x_right - x_left);
			if(z < wbuf[(int)(x + xadj + (y * SCREEN_W))]) {
				FILLBMP;
				wbuf[(int)(x + xadj + (y * SCREEN_W))] = z;
			}
		}
		x_left += dx_left;
		x_right += dx_right;
		z_left += dz_left;
		z_right += dz_right;
	}
}

static void draw_triangle(u32* bmp, double* wbuf,
			  double x1, double y1, double z1,
			  double x2, double y2, double z2,
			  double x3, double y3, double z3,
			  u32 fill, int side)
{
	double tmp, leftx, rightx, leftz, rightz;
	SWAPPING(x2, y2, z2, x1, y1, z1)
	SWAPPING(x3, y3, z3, x1, y1, z1)
	SWAPPING(x3, y3, z3, x2, y2, z2)

	if(y1 == y2) {
		draw_bottomtriangle(bmp, wbuf, x1, y1, z1, x2, y2, z2, x3, y3, z3, fill, side);
	}else if(y2 == y3) {
		draw_toptriangle(bmp, wbuf, x1, y1, z1, x2, y2, z2, x3, y3, z3, fill, side);
	}else{
		leftx = x1 + (y2 - y1) * (x3 - x1) / (y3 - y1);
		leftz = z1 + (y2 - y1) * (z3 - z1) / (y3 - y1);
		rightx = x2;
		rightz = z2;
		if(leftx > rightx) {
			SWAP(leftx, rightx)
			SWAP(leftz, rightz)
		}
		draw_toptriangle(bmp, wbuf, x1, y1, z1, leftx, y2, leftz, rightx, y2, rightz, fill, side);
		draw_bottomtriangle(bmp, wbuf, leftx, y2, leftz, rightx, y2, rightz, x3, y3, z3, fill, side);
	}
}

static void _video_draw_core(double x1, double y1, double z1,
			     double x2, double y2, double z2,
			     double x3, double y3, double z3,
			     u32 fill, int side)
{
	draw_triangle(vbmp, zbuffer, x1, y1, z1, x2, y2, z2, x3, y3, z3, fill, side);
}

void Video_Wires(double coords[3][6], u32 fill)
{
	Video_Line(coords[0][0], coords[0][1], 
		   coords[1][0], coords[1][1], fill, 0);
	Video_Line(coords[1][0], coords[1][1], 
		   coords[2][0], coords[2][1], fill, 0);
	Video_Line(coords[2][0], coords[2][1], 
		   coords[0][0], coords[0][1], fill, 0);
	if(_stereo) {
		Video_Line(coords[0][3], coords[0][4], 
			   coords[1][3], coords[1][4], fill, 1);
		Video_Line(coords[1][3], coords[1][4], 
			   coords[2][3], coords[2][4], fill, 1);
		Video_Line(coords[2][3], coords[2][4], 
			   coords[0][3], coords[0][4], fill, 1);
	}
}

void Video_Draw(double coords[3][6], u32 fill)
{
	_video_draw_core(coords[0][0], coords[0][1], coords[0][2],
			 coords[1][0], coords[1][1], coords[1][2],
			 coords[2][0], coords[2][1], coords[2][2],
			 fill, 0);
	if(_stereo) {
		_video_draw_core(coords[0][3], coords[0][4], coords[0][5],
				 coords[1][3], coords[1][4], coords[1][5],
				 coords[2][3], coords[2][4], coords[2][5],
				 fill, 1);
	}
}

static void clear_zbuf()
{
	int i, x;
	for(i = 0; i < SCREEN_H; i++) {
		for(x = 0; x < SCREEN_W; x++) {
			zbuffer[(i * SCREEN_W) + x] = DBL_MAX;
		}
	}
}	

void Video_Update()
{
#if VIDEO_THROTTLE
	/* TODO: this */
#endif
	if(SDL_LockSurface(screen) == -1) {
		fprintf(stderr, "Couldn't lock blitting surface: %s.\n", SDL_GetError());
		return;
	}
	int i, x;
	if(_stereo) {
		for(x = (SCREEN_W/4)-10; x < (SCREEN_W/4)+10; x++) {
			for(i = HEIGHT+10; i < HEIGHT+30; i++) {
				vbmp[(i * SCREEN_W) + x] = 0xFFFFFFFF;
			}
		}
		for(x = (SCREEN_W*3/4)-10; x < (SCREEN_W*3/4)+10; x++) {
			for(i = HEIGHT+10; i < HEIGHT+30; i++) {
				vbmp[(i * SCREEN_W) + x] = 0xFFFFFFFF;
			}
		}
	}
	clear_zbuf();
	memcpy(screen->pixels, vbmp, SCREEN_W * SCREEN_H * 4);
	SDL_UnlockSurface(screen);
	if(SDL_Flip(screen) == -1) {
		fprintf(stderr, "Couldn't flip screen: %s\n", SDL_GetError());
		return;
	}
	Video_Clear();
}

void Video_Clear()
{
	bzero(vbmp, SCREEN_W * SCREEN_H * 4);
}

